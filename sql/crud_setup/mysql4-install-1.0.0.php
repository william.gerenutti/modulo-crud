<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();
$installer->run("

	CREATE TABLE IF NOT EXISTS bis2bis_crud(
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(250),
    cidade VARCHAR(50),
	cpf VARCHAR(12),
	email VARCHAR(200),
	estado_civil INTEGER(1)
	);

");

$installer->endSetup();

