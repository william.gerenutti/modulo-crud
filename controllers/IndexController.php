<?php
class BIS2BIS_Crud_IndexController extends Mage_Core_Controller_Front_Action{
    public function indexAction(){
        $this->loadLayout();     
        $this->renderLayout();
    }

    public function createAction(){
        $this->loadLayout();     
        $this->renderLayout();
    }

    public function saveAction(){

        $nome = $this->getRequest()->getPost('nome');
        $cidade = $this->getRequest()->getPost('cidade');  
        $cpf = $this->getRequest()->getPost('cpf');  
        $email = $this->getRequest()->getPost('email');  
        $estadoCivil = $this->getRequest()->getPost('estadoCivil');  

        $magento = Mage::getModel('crud/crud');

        $magento->setData('nome', $nome);
        $magento->setData('cidade', $cidade);
        $magento->setData('cpf', $cpf);
        $magento->setData('email', $email);
        $magento->setData('estado_civil', $estadoCivil);

        $magento->save(); 
        Mage::getSingleton('core/session')->addSuccess('Cliente cadastrado com sucesso!');   
        $this->_redirect('crud/index');
    }

    public function editAction(){
        $this->loadLayout();     
        $this->renderLayout();
    }
    
    public function updateAction(){

        $id = $this->getRequest()->getParam('id');      
        $params = Mage::getModel('crud/crud')->load($id);
        $data = $this->getRequest()->getPost();

        $params->setData('nome',$data['nome']);
        $params->setData('cidade',$data['cidade']);
        $params->setData('cpf',$data['cpf']);
        $params->setData('email',$data['email']);
        $params->setData('estado_civil',$data['estadoCivil']);
        $params->save();
        Mage::getSingleton('core/session')->addSuccess('Cliente editado com sucesso!');
        $this->_redirect('crud/index');
    }

    public function deleteAction(){

        $id = $this->getRequest()->getParam('id');

        $params = Mage::getModel('crud/crud')->load($id);
        $params->delete();
        Mage::getSingleton('core/session')->addSuccess('Cliente deletado com sucesso!');
        $this->_redirect('*/*/');
    } 
    
    // retorna sessao
	public function _getSessao(){
		return Mage::getSingleton('adminhtml/session');
	}
}