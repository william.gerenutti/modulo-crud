<?php
class BIS2BIS_Crud_Adminhtml_Crud_AdminController extends Mage_Adminhtml_Controller_Action {

    public function _initAction() {
		$this->loadLayout()->_addBreadcrumb(Mage::helper('crud')->__('Crud'), Mage::helper('crud')->__('Crud'));
		return $this;
	}

    public function listaAction(){
		$this->_initAction()->_addContent($this->getLayout()->createBlock('crud/Adminhtml_Lista'));
		$this->renderLayout();
	}

	//  tela de cadastro
	public function cadastrarAction(){
		$this->_initAction()->_addContent($this->getLayout()->createBlock('crud/Adminhtml_Cadastro'));
		$this->renderLayout();
	}

	public function salvarAction(){

		$params = $this->getRequest()->getParams();

		$id = $params['id'];
		$nome = $params['nome'];
		$cidade = $params['cidade'];
		$cpf = $params['cpf'];
		$email = $params['email'];
		$estadoCivil = $params['estadoCivil'];

		$crud = Mage::getModel('crud/crud');

		if($id == "" ){
			$crud->setData('nome', $nome);
			$crud->setData('cidade', $cidade);
			$crud->setData('cpf', $cpf);
			$crud->setData('email', $email);
			$crud->setData('estado_civil', $estadoCivil);
			$crud->save();
			
			Mage::app()->getCacheInstance()->flush();
			Mage::getSingleton('adminhtml/session')->addSuccess('Operação Realizada com Sucesso !');
			$this->_redirect('*/*/lista');
		}else{
			$idCrud = $this->getRequest()->getParam('id');
			$parametro = Mage::getModel('crud/crud')->load($idCrud);
			$data = $this->getRequest()->getPost();

			$parametro->setData('nome', $data['nome']);
			$parametro->setData('cidade', $data['cidade']);
			$parametro->setData('cpf', $data['cpf']);
			$parametro->setData('email', $data['email']);
			$parametro->setData('estado_civil', $data['estadoCivil']);
			$parametro->save();

			Mage::app()->getCacheInstance()->flush();
			Mage::getSingleton('adminhtml/session')->addSuccess('Operação Realizada com Sucesso !');
			$this->_redirect('*/*/lista');
		}
	}

	public function editarAction(){
		$params = $this->getRequest()->getParams();
		$id = $params['id'];
		$crud = Mage::getModel('crud/crud')->load($id);
		Mage::register('id', $id);
		Mage::register('nome', $crud->getNome());
		Mage::register('cidade', $crud->getCidade());
		Mage::register('cpf', $crud->getCpf());
		Mage::register('email', $crud->getEmail());
		Mage::register('estadoCivil', $crud->getEstadoCivil());
		$this->_initAction()->_addContent($this->getLayout()->createBlock('crud/Adminhtml_Cadastro'));
		$this->renderLayout();
	}

	public function deletarAction(){
		$params = $this->getRequest()->getParams();
		$id = $params['id'];
		$crud = Mage::getModel('crud/crud')->load($id);
		$crud->load($id);
		$crud->delete();

		Mage::app()->getCacheInstance()->flush();
		Mage::getSingleton('adminhtml/session')->addSuccess('Usuário deletado com Sucesso!');
		$this->_redirect('*/*/lista');
	}
}