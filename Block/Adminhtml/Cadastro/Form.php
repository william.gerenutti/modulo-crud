<?php

class BIS2BIS_Crud_Block_Adminhtml_Cadastro_Form extends Mage_Adminhtml_Block_Widget_Form{

    protected function _prepareForm(){

        $form = new Varien_Data_Form(array(
			'id' => 'edit_form',
			'action' => Mage::helper('adminhtml')->getUrl('*/*/salvar'),
			'method' => 'post',
            'enctype' => 'multipart/form-data'
		));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => Mage::helper('crud')->__('Cadastro de pessoas')
        ));

        $fieldset->addField('id', 'hidden', array(
            'value' => Mage::registry('id'),
            'name'      => 'id'
        ));

        $fieldset->addField('nome', 'text', array(
            'label'     => Mage::helper('crud')->__('Nome'),
            'required'  => true,
            'value' => Mage::registry('nome'),
            'name'      => 'nome',
            'after_element_html' => '<small>Nome do usuario (Ex: William Gerenutti)</small>'
        ));

        $fieldset->addField('cidade', 'text', array(
            'label'     => Mage::helper('crud')->__('Cidade'),
            'required'  => true,
            'value' => Mage::registry('cidade'),
            'name'      => 'cidade',
            'after_element_html' => '<small>Nome da cidade (Ex: Londrina)</small>'
        ));

        $fieldset->addField('cpf', 'text', array(
            'label'     => Mage::helper('crud')->__('CPF'),
            'required'  => true,
            'value' => Mage::registry('cpf'),
            'name'      => 'cpf',
            'after_element_html' => '<small>CPF(Ex: 00000000000)</small>'
        ));

        $fieldset->addField('email', 'text', array(
            'label'     => Mage::helper('crud')->__('E-mail'),
            'required'  => true,
            'value' => Mage::registry('email'),
            'name'      => 'email',
            'after_element_html' => '<small>CPF(Ex: usuarui@usuario.com)</small>'
        ));

        $fieldset->addField('estadoCivil', 'select', array(
            'label'     => Mage::helper('crud')->__('Estado Civil'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'estadoCivil',
            'values' => array('0'=>'Solteiro','1' => 'Casado', '2' => 'Divorciado'),
            'tabindex' => 1,
            'value' => Mage::registry('estadoCivil')
        ));
        

        $form->setUseContainer(true);
	    $this->setForm($form);
		return parent::_prepareForm();
    }
}