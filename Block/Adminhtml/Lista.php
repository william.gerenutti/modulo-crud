<?php

class BIS2BIS_Crud_Block_Adminhtml_Lista  extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct(){
        
        $this->_blockGroup = 'crud';
        $this->_controller = 'Adminhtml_Lista';
        $this->_headerText = Mage::helper('crud')->__('Lista de Crud');
        parent::__construct();

        $this->_addButton('adicionar_usuario', array(
            'label'     => Mage::helper('crud')->__('Adicionar Usuario'),
            'onclick' => "setLocation('{$this->getUrl('*/*/cadastrar')}')",
            'class'     => 'download'
        ), 0, 100, 'header', 'header');
        
        $this->_removeButton('add');
    }
}

?>