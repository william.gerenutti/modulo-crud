<?php

class BIS2BIS_Crud_Block_Adminhtml_Cadastro extends Mage_Adminhtml_Block_Widget_Form_Container{
    
    protected function _prepareLayout()
    {
        $this->setChild('form',$this->getLayout()->createBlock('crud/Adminhtml_Cadastro_Form'));
        return parent::_prepareLayout();
    }

    public function construct(){
        $this->_objectId = 'id';
        $this->_blockGroup = 'crud';
        $this->_controller = 'adminhtml_cadastro';
        parent::__construct();
        $this->updateButton('save', 'label', 'Salvar usuário');
    }

    public function getHeaderText(){
    	return 'Cadastro de usuário';
	}

    public function getDeleteUrl(){
        return $this->getUrl('*/*/deletar', array('_current'=>true, 'back'=>null));
    }

    public function getBackUrl(){
    	return $this->getUrl('*/*/lista', array('_current'=>true, 'back'=>null));
    }
}