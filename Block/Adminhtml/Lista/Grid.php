<?php
class BIS2BIS_Cud_Block_Adminhtml_Lista_Grid  extends Mage_Adminhtml_Block_Widget_Grid{
    public function __construct(){
        parent::__construct();
        $this->setId('gridLista');
    }

    protected function _prepareCollection(){
        $collection = Mage::getModel('crud/crud')->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();
	    return $this;
    }

    protected function _prepareColumns(){
        $this->addColumn('id', array(
           'header' => 'ID',
           'sortable' => true,
           'index' => 'id' 
        ));

        $this->addColumn('nome', array(
           'header' => 'Nome',
           'sortable' => true,
           'index' => 'nome' 
        ));

        $this->addColumn('cidade', array(
            'header' => 'Cidade',
            'sortable' => true,
            'index' => 'cidade' 
         ));

        $this->addColumn('cpf', array(
            'header' => 'Cpf',
            'sortable' => true,
            'index' => 'cpf' 
        ));
        
        $this->addColumn('email', array(
            'header' => 'Email',
            'sortable' => true,
            'index' => 'email' 
        ));

        $this->addColumn('estadoCivil', array(
            'header' => 'Estado Civil',
            'sortable' => true,
            'index' => 'estado_civil' 
        ));

        return parent::_prepareColumns();
    }
    
    public function getRowUrl($row){
        return $this->getUrl('*/*/editar', array('id' => $row->getId()));
    }

}
